//
//  TeamTableViewController.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by Daniel Favor on 3/16/19.
//  Copyright © 2019 Daniel Favor. All rights reserved.
//

import UIKit

class TeamTableViewController: UITableViewController {
    var school:School?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return school!.teams.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teamCell")
        cell?.textLabel?.text = school?.teams[indexPath.row].name
        return cell!
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "teamToNewTeam"){
            let teamDVC = segue.destination as! NewTeamViewController
            teamDVC.school = school
        }
        if(segue.identifier == "teamToStudents"){
            let studentDVC = segue.destination as! StudentsViewController
            studentDVC.team = school?.teams[tableView.indexPathForSelectedRow!.row]
        }
    }
}

