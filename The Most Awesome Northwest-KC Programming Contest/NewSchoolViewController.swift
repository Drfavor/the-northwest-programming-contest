//
//  NewSchoolViewController.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by Daniel Favor on 3/14/19.
//  Copyright © 2019 Daniel Favor. All rights reserved.
//

import UIKit

class NewSchoolViewController: UIViewController {
    
    @IBOutlet weak var nameTF: UITextField!
    
    @IBOutlet weak var coachTF: UITextField!
    
    
    @IBAction func CancelBTN(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func done(_ sender: Any) {
        Schools.shared.add(school: School(name:nameTF.text!, coach:coachTF.text!))
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
