//
//  SchoolsViewController.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by Daniel Favor on 3/16/19.
//  Copyright © 2019 Daniel Favor. All rights reserved.
//

import UIKit

class SchoolsTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Schools.shared.numSchools()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "schoolCell")
        cell?.textLabel?.text = Schools.shared.schools[indexPath.row].name
        cell?.detailTextLabel?.text = "\(Schools.shared.schools[indexPath.row].coach)"
        return cell!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "schoolToTeam"){
            let schoolDVC = segue.destination as! TeamTableViewController
            schoolDVC.school = Schools.shared.schools[tableView.indexPathForSelectedRow!.row]
        }
    }

}
