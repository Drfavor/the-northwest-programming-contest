//
//  Schools.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by Daniel Favor on 3/16/19.
//  Copyright © 2019 Daniel Favor. All rights reserved.
//

import Foundation
class Team : CustomStringConvertible, Equatable{
    var name:String
    var members:[String]
    
    var description: String {
        return "\(name) -- \(members)"
    }
    static func == (lhs: Team, rhs: Team) -> Bool {
        return lhs.name == rhs.name
    }
    init(name:String, members:[String]){
        self.name = name
        self.members = members
    }
    
}

class School : CustomStringConvertible, Equatable {
    var name:String
    var teams:[Team] = []
    var coach:String
    static func == (lhs: School, rhs: School) -> Bool {
        return lhs.name == rhs.name
    }

    var description: String {
        return "\(name) -- \(teams)"
    }
    init(name:String, coach:String){
        self.name = name
        self.coach = coach
        self.teams = []
    }
    func addTeam(team:Team){
        teams.append(team)
    }
}

class Schools {
    
    static var shared = Schools()
    
    public var schools:[School] = []
    
    private init(){}
    
    func numSchools()->Int {
        return schools.count
    }
    
    subscript(index:Int) -> School {
        return schools[index]
    }
    
    func add(school:School){
        schools.append(school)
    }
    
}

