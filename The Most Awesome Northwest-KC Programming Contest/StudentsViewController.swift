//
//  StudentsViewController.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by Daniel Favor on 3/14/19.
//  Copyright © 2019 Daniel Favor. All rights reserved.
//

import UIKit

class StudentsViewController: UIViewController {

    @IBOutlet weak var Student0LBL: UILabel!
    @IBOutlet weak var Student1LBL: UILabel!
    @IBOutlet weak var Student2LBL: UILabel!
    var team:Team!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Student0LBL.text = team.members[0]
        Student1LBL.text = team.members[1]
        Student2LBL.text = team.members[2]
    }
}
