//
//  NewTeamViewController.swift
//  The Most Awesome Northwest-KC Programming Contest
//
//  Created by Daniel Favor on 3/14/19.
//  Copyright © 2019 Daniel Favor. All rights reserved.
//

import UIKit

class NewTeamViewController: UIViewController {
    var school:School!
    
    @IBOutlet weak var TeamTF: UITextField!
    
    @IBOutlet weak var Student0TF: UITextField!
    @IBOutlet weak var Student1TF: UITextField!
    @IBOutlet weak var Student2TF: UITextField!
    

    @IBAction func cancelBTN(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBTN(_ sender: Any) {
        school.addTeam(team: Team(name: TeamTF.text!, members: [Student0TF.text!, Student1TF.text!,Student2TF.text!]))
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
}
